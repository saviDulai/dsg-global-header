﻿using SourceCode.Forms.Controls.Web.SDK;
using SourceCode.Forms.Controls.Web.SDK.Attributes;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("Tempus.Forms.Controls.MyStateGlobal.AllStyles.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Tempus.Forms.Controls.Radios.RadiosScript.js", "text/javascript", PerformSubstitution = true)]

namespace Tempus.Forms.Controls
{
    [ControlTypeDefinition("Tempus.Forms.Controls.Radios.RadiosDefinition.xml")]
    [ClientScript("Tempus.Forms.Controls.Radios.RadiosScript.js")]
    [ClientCss("Tempus.Forms.Controls.MyStateGlobal.AllStyles.css")]

    class Radios : BaseControl
    {
        #region properties
        public bool IsVisible
        {
            get
            {
                return this.GetOption<bool>("isvisible", true);
            }
            set
            {
                this.SetOption<bool>("isvisible", value, true);
            }
        }

        //IsEnabled property
        public bool IsEnabled
        {
            get
            {
                return this.GetOption<bool>("isenabled", true);
            }
            set
            {
                this.SetOption<bool>("isenabled", value, true);
            }
        }


        #endregion

        //constructor
        public Radios(): base("div") 
        {
            //((SourceCode.Forms.Controls.Web.Shared.IControl)this).DesignFormattingPaths.Add("stlyecss", "SourceCodeANZ.Forms.Controls.Resources.MaterialControlStyle.css");
        }

        protected override void CreateChildControls()
        {
            if (this.State == SourceCode.Forms.Controls.Web.Shared.ControlState.Designtime)
            {
                Label lbl = new Label();
                lbl.Text = "Radios";
                lbl.Style["padding-left"] = "2px";
                this.Controls.Add(lbl);
             }
            else if (this.State == SourceCode.Forms.Controls.Web.Shared.ControlState.Runtime)
            {

            }
        }

    }
}