﻿(function ($) {

    if (typeof Tempus === "undefined" || Tempus == null) Tempus = {};
    if (typeof Tempus.Forms === "undefined" || Tempus.Forms == null) Tempus.Forms = {};
    if (typeof Tempus.Forms.Controls === "undefined" || Tempus.Forms.Controls == null) Tempus.Forms.Controls = {};

    Tempus.Forms.Controls.Radios = {
        //internal method used to get a handle on the control instance
        _getInstance: function (id) {
            var control = jQuery('#' + id);
            if (control.length == 0) {
                throw 'Radios \'' + id + '\' not found';
            } else {
                return control[0];
            }
        },

        getProperty: function (objInfo) {
            if (objInfo.property.toLowerCase() == "value") {
                return SourceCodeANZ.Forms.Controls.Menu.getValue(objInfo);
            }
            else if (objInfo.property.toLowerCase() == "isvisible") {
                Tempus.Forms.Controls.Radios.setIsVisible(objInfo);
            }
            else if (objInfo.property.toLowerCase() == "isenabled") {
                Tempus.Forms.Controls.Radios.setIsEnabled(objInfo);
            }
            else {
                var jqData = $('#' + objInfo.CurrentControlId).data('options');
                return jqData[objInfo.property.toLowerCase()];
            }
        },

        _setPropertyValue: function (controlId, fieldName, value) {
            var jqData = $('#' + controlId).data('options');
            jqData[fieldName] = value;
            $('#' + controlId).data('options', jqData);
        },

        setProperty: function (objInfo) {
            switch (objInfo.property.toLowerCase()) {
                case "isvisible":
                    Tempus.Forms.Controls.Radios.setIsVisible(objInfo);
                    break;

                case "isenabled":
                    Tempus.Forms.Controls.Radios.setIsEnabled(objInfo);
                    break;

                default:
                    {
                        Tempus.Forms.Controls.Radios._setPropertyValue(objInfo.CurrentControlId, objInfo.property.toLowerCase(), objInfo.Value);
                        $('#' + objInfo.CurrentControlId).find('#' + objInfo.property.toLowerCase()).val(objInfo.Value);
                    }
            }
        },

        //helper method to set visibility
        setIsVisible: function (objInfo) {
            value = (objInfo.Value === true || objInfo.Value == 'true');
            this._isVisible = value;
            var displayValue = (value === false) ? "none" : "block";
            var instance = Tempus.Forms.Controls.Radios._getInstance(objInfo.CurrentControlId);
            instance.style.display = displayValue;
        },

        //helper method to set control "enabled" state
        setIsEnabled: function (objInfo) {
            value = (objInfo.Value === true || objInfo.Value == 'true');
            this._isEnabled = value;
            var instance = Tempus.Forms.Controls.Radios._getInstance(objInfo.CurrentControlId);
            instance.readOnly = !value;
        },

        //setStyles: function (wrapper, styles, target) {
        //    var isRuntime = (wrapper == null);
        //    var options = {};
        //    var element = isRuntime ? jQuery(target) : wrapper.find('.Tempus.Forms.Controls.Radios');

        //    jQuery.extend(options, {
        //        "border": element,
        //        "background": element,
        //        "margin": element,
        //        "padding": element,
        //        "font": element,
        //        "horizontalAlign": element
        //    });

        //    StyleHelper.setStyles(options, styles);
        //},

        executeMethod: function (objInfo) {
            switch (objInfo.methodName) {
                case "FadeIn":
                    var fadeTime = 1000;
                    if (objInfo.methodParameters.time)
                        fadeTime = parseInt(objInfo.methodParameters.time);
                    $('#' + objInfo.CurrentControlId).fadeIn(fadeTime);
                    break;
                case "FadeOut":
                    var fadeTime = 1000;
                    if (objInfo.methodParameters.time)
                        fadeTime = parseInt(objInfo.methodParameters.time);
                    $('#' + objInfo.CurrentControlId).fadeOut(fadeTime);
                    break;
            }
        },

        _attachEventHandler: function ($this) {
            var controlid = $this.attr('id')

            $this.on('click', function () {
                raiseEvent(controlid, 'Control', 'OnClick')
            });

            //$this.find('#FIELDNAME').on('change', function () {
            //    var updatedValue = $this.find('#FIELDNAME').val();
            //    Tempus.Forms.Controls.MyStateTaxInformation._setPropertyValue(controlid, 'FIELDNAME', updatedValue);
            //});
        }
    }
})(jQuery)

$(document).ready(function () {

    $('.SFC.Tempus-Forms-Controls-Radios').each(function (i, element) {
        var $this = $(element)

        var str = '<div class="question"><label class="question">Select your terms</label></div>' +
            '<div class="inline-radio full-width"><input name="gender" type="radio" value="2.35" class="iradio_square-grey"/> <strong>2.35</strong>%p.a for 3 months</div>' +
            '<div class="inline-radio full-width"><input name="gender" type="radio" value="2.50" class="iradio_square-grey"/> <strong>2.50</strong>%p.a for 6 months</div>' +
            '<div class="inline-radio full-width"><input name="gender" type="radio" value="2.75" class="iradio_square-grey"/> <strong>2.75</strong>%p.a for 12 months</div>' +
            '<p>show more terms.</p>'

        var html = $.parseHTML(str)
        $(html).appendTo($this)

        Tempus.Forms.Controls.Radios._attachEventHandler($this)

    });

});