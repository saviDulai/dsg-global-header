﻿using System.Configuration;
using System.Collections.Specialized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
//import ADO.NET provider 
using SourceCode.Data.SmartObjectsClient;

namespace Tempus.Forms.Controls.DSG.SRC
{
    public static class Utilities

    {
        //Sample: building a connection string to K2 smartobject server
        public static SOConnection EstablishK2Connection()
        {
            // Retrieve app settings from K2HostConfig.config
            //string sAttrHostName;       // config k2 host name
            //int sAttrHostPort;          // config k2 host port
            //sAttrHostName = ConfigurationManager.AppSettings.Get("K2HostName");
            //sAttrHostPort = int.Parse(ConfigurationManager.AppSettings.Get("K2HostPort"));

            SOConnectionStringBuilder hostServerConnectionString = new SOConnectionStringBuilder();
            hostServerConnectionString.Server = "localhost";  // Connect string K2 host name
            hostServerConnectionString.Port = 5555;    // Connect string K2 host port

            SOConnection connection = new SOConnection(hostServerConnectionString.ToString());
            return connection;
        }

    }
}
