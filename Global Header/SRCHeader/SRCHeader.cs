﻿using SourceCode.Forms.Controls.Web.SDK;
using SourceCode.Forms.Controls.Web.SDK.Attributes;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("Tempus.Forms.Controls.DSG.SRC.SRCGlobal.SRCStyle.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Tempus.Forms.Controls.DSG.SRC.SRCGlobal.w3.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Tempus.Forms.Controls.DSG.SRC.SRCHeader.SRCHeader.js", "text/javascript", PerformSubstitution = true)]

namespace Tempus.Forms.Controls.DSG.SRC
{
    [ControlTypeDefinition("Tempus.Forms.Controls.DSG.SRC.SRCHeader.SRCHeader.xml")]
    [ClientCss("Tempus.Forms.Controls.DSG.SRC.SRCGlobal.SRCStyle.css")]
    [ClientScript("Tempus.Forms.Controls.DSG.SRC.SRCHeader.SRCHeader.js")]
    [ClientCss("Tempus.Forms.Controls.DSG.SRC.SRCGlobal.w3.css")]

    class SRCHeader : BaseControl
    {
        #region properties
        public bool IsVisible
        {
            get
            {
                return this.GetOption<bool>("isvisible", true);
            }
            set
            {
                this.SetOption<bool>("isvisible", value, true);
            }
        }

         public bool IsEnabled
        {
            get
            {
                return this.GetOption<bool>("isenabled", true);
            }
            set
            {
                this.SetOption<bool>("isenabled", value, true);
            }
        }

        #endregion
        //constructor
        public SRCHeader(): base("div") 
        {

        }

        protected override void CreateChildControls()
        {
            this.Attributes.CssStyle.Add(HtmlTextWriterStyle.Width, "100%");
            if (this.State == SourceCode.Forms.Controls.Web.Shared.ControlState.Designtime)
            {
                Label lbl = new Label();
                lbl.Text = "SRCHeader";
                lbl.Style["padding-left"] = "2px";
                
                this.Controls.Add(lbl);
             }
            else if (this.State == SourceCode.Forms.Controls.Web.Shared.ControlState.Runtime)
            {

            }
        }

    }
}