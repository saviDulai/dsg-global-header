﻿using SourceCode.Forms.Controls.Web.SDK;
using SourceCode.Forms.Controls.Web.SDK.Attributes;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("Tempus.Forms.Controls.DSG.SRC.SRCGlobal.SRCStyle.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Tempus.Forms.Controls.DSG.SRC.SRCGlobal.w3.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons.SRCWorkflowActionsButtons.js", "text/javascript", PerformSubstitution = true)]

namespace Tempus.Forms.Controls.DSG.SRC
{
    [ControlTypeDefinition("Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons.SRCWorkflowActionsButtons.xml")]
    [ClientCss("Tempus.Forms.Controls.DSG.SRC.SRCGlobal.SRCStyle.css")]
    [ClientScript("Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons.SRCWorkflowActionsButtons.js")]
    [ClientCss("Tempus.Forms.Controls.DSG.SRC.SRCGlobal.w3.css")]

    class SRCWorkflowActionsButtons : BaseControl
    {
        #region properties
        public bool IsVisible
        {
            get
            {
                return this.GetOption<bool>("isvisible", true);
            }
            set
            {
                this.SetOption<bool>("isvisible", value, true);
            }
        }

        public bool IsEnabled
        {
            get
            {
                return this.GetOption<bool>("isenabled", true);
            }
            set
            {
                this.SetOption<bool>("isenabled", value, true);
            }
        }

        #endregion
        //constructor
        public SRCWorkflowActionsButtons() : base("div")
        {

        }

        protected override void CreateChildControls()
        {
            this.Attributes.CssStyle.Add(HtmlTextWriterStyle.Width, "100%");
            if (this.State == SourceCode.Forms.Controls.Web.Shared.ControlState.Designtime)
            {
                Label lbl = new Label();
                lbl.Text = "SRCWorkflowActionsButtons";
                lbl.Style["padding-left"] = "2px";

                this.Controls.Add(lbl);
            }
            else if (this.State == SourceCode.Forms.Controls.Web.Shared.ControlState.Runtime)
            {

            }
        }

    }
}