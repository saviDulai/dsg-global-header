﻿(function ($) {

    if (typeof Tempus === "undefined" || Tempus == null) Tempus = {};
    if (typeof Tempus.Forms === "undefined" || Tempus.Forms == null) Tempus.Forms = {};
    if (typeof Tempus.Forms.Controls === "undefined" || Tempus.Forms.Controls == null) Tempus.Forms.Controls = {};
    if (typeof Tempus.Forms.Controls.DSG === "undefined" || Tempus.Forms.Controls.DSG == null) Tempus.Forms.Controls.DSG = {};
    if (typeof Tempus.Forms.Controls.DSG.SRC === "undefined" || Tempus.Forms.Controls.DSG.SRC == null) Tempus.Forms.Controls.DSG.SRC = {};

   Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons = {
        //internal method used to get a handle on the control instance
        _getInstance: function (id) {
            var control = jQuery('#' + id);
            if (control.length == 0) {
                throw 'SRCWorkflowActionsButtons \'' + id + '\' not found';
            } else {
                return control[0];
            }
        },

        getProperty: function (objInfo) {
            if (objInfo.property.toLowerCase() == "value") {
                return SourceCodeANZ.Forms.Controls.Menu.getValue(objInfo);
            }
            else if (objInfo.property.toLowerCase() == "isvisible") {
               Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons.setIsVisible(objInfo);
            }
            else if (objInfo.property.toLowerCase() == "isenabled") {
               Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons.setIsEnabled(objInfo);
            }
            else {
                var jqData = $('#' + objInfo.CurrentControlId).data('options');
                return jqData[objInfo.property.toLowerCase()];
            }
        },

        _setPropertyValue: function (controlId, fieldName, value) {
            var jqData = $('#' + controlId).data('options');
            jqData[fieldName] = value;
            $('#' + controlId).data('options', jqData);
        },

        setProperty: function (objInfo) {
            switch (objInfo.property.toLowerCase()) {
                case "isvisible":
                   Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons.setIsVisible(objInfo);
                    break;

                case "isenabled":
                   Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons.setIsEnabled(objInfo);
                    break;

                default:
                    {
                       Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons._setPropertyValue(objInfo.CurrentControlId, objInfo.property.toLowerCase(), objInfo.Value);
                        $('#' + objInfo.CurrentControlId).find('#' + objInfo.property.toLowerCase()).val(objInfo.Value);
                    }
            }
        },

        //helper method to set visibility
        setIsVisible: function (objInfo) {
            value = (objInfo.Value === true || objInfo.Value == 'true');
            this._isVisible = value;
            var displayValue = (value === false) ? "none" : "block";
            var instance =Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons._getInstance(objInfo.CurrentControlId);
            instance.style.display = displayValue;
        },

        //helper method to set control "enabled" state
        setIsEnabled: function (objInfo) {
            value = (objInfo.Value === true || objInfo.Value == 'true');
            this._isEnabled = value;
            var instance =Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons._getInstance(objInfo.CurrentControlId);
            instance.readOnly = !value;
        },

        //setStyles: function (wrapper, styles, target) {
        //    var isRuntime = (wrapper == null);
        //    var options = {};
        //    var element = isRuntime ? jQuery(target) : wrapper.find('.Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons');

        //    jQuery.extend(options, {
        //        "border": element,
        //        "background": element,
        //        "margin": element,
        //        "padding": element,
        //        "font": element,
        //        "horizontalAlign": element
        //    });

        //    StyleHelper.setStyles(options, styles);
        //},

        executeMethod: function (objInfo) {
            switch (objInfo.methodName) {
                case "FadeIn":
                    $('#' + objInfo.CurrentControlId).closest('.row').show();
                    $('#' + objInfo.CurrentControlId).closest('.formcontrol').show();
                    var fadeTime = 1000;
                    if (objInfo.methodParameters.time)
                        fadeTime = parseInt(objInfo.methodParameters.time);
                    $('#' + objInfo.CurrentControlId).fadeIn(fadeTime);
                    break;
                case "FadeOut":
                    var fadeTime = 1000;
                    if (objInfo.methodParameters.time)
                        fadeTime = parseInt(objInfo.methodParameters.time);
                    $('#' + objInfo.CurrentControlId).fadeOut(fadeTime);
                    $('#' + objInfo.CurrentControlId).closest('.formcontrol').hide();
                    $('#' + objInfo.CurrentControlId).closest('.row').hide();
                    break;
                case "SetErrorMessage":
                    var messageFlag = objInfo.methodParameters.messageflag;
                    if (messageFlag == "YES") {
                        var message = objInfo.methodParameters.message;
                        $('#error-message-container').show();
                        $('#error-message-body').text(message);
                    } else {
                        $('#error-message-container').hide();
                        $('error-#message-body').text("");
                    }
                    break;
                case "SetMessage":
                    var messageFlag = objInfo.methodParameters.messageflag;
                    if (messageFlag == "YES") {
                        var message = objInfo.methodParameters.message;
                        $('#message-container').show();
                        $('#message-body').text(message);
                    } else {
                        $('#message-container').hide();
                        $('#message-body').text("");
                    }
                    break;
                case "ShowNextButton":
                    $('#next').show();
                    break;
                case "ShowSubmitButton":
                    $('#submit').show();
                    break;
                case "ShowBackButton":
                    $('#back').show();
                    break;
                case "HideNextButton":
                    $('#next').hide();
                    break;
                case "HideSubmitButton":
                    $('#submit').hide();
                    break;
                case "HideBackButton":
                    $('#back').hide();
                    break;
            }
        },

        _attachEventHandler: function ($this) {
            var controlid = $this.attr('id')

            $this.on('click', function (event) {
                debugger;
                //console.log(event);

                //determine which button - and then raise each event - depending
                if (event.target.id == 'back') {
                    raiseEvent(controlid, 'Control', 'OnBackButtonClick');
                }
                else if (event.target.id == 'submit') {
                    raiseEvent(controlid, 'Control', 'OnSubmitButtonClick');
                }
                else if (event.target.id == 'next') {
                    raiseEvent(controlid, 'Control', 'OnNextButtonClick');
                }
            })


            $('#back').click(function () {
                $(window).scrollTop(0);
            });

            $('#next').click(function () {
                $(window).scrollTop(0);
            });

            $('#submit').click(function () {
                $(window).scrollTop(0);
            });
            //$this.find('#FIELDNAME').on('change', function () {
            //    var updatedValue = $this.find('#FIELDNAME').val();
            //    Tempus.Forms.Controls.MyStateTaxInformation._setPropertyValue(controlid, 'FIELDNAME', updatedValue);
            //});
        }
    }
})(jQuery)


$(document).ready(function () {

    $('.SFC.Tempus-Forms-Controls-DSG-SRC-SRCWorkflowActionsButtons').each(function (i, element) {
        var $this = $(element)

        var str =
'            <div id="workflowaction-container" class="w3-container"> ' +
'                <div id="message-container" style="display:none;"> ' +
'                   <div class="w3-col s12 m12 l12"> ' +
'                         <p id="message-body"></p>' +
'                    </div> ' +
'                </div> ' +
'                <div id="error-message-container" style="display:none;"> ' +
'                   <div class="w3-col s12 m12 l12"> ' +
'                         <p id="error-message-body"></p>' +
'                    </div> ' +
'                </div> ' +
'                <div class="w3-row"> ' +
'                    <div id="action-button-container-left" class="w3-col s12 m6 l6"> ' +
'                        <div id="back-button-container" class="w3-col s12 m6 l6"> ' +
'                            <button id="back" class="action-buttons">Back</button> ' +
'                        </div> ' +
'                    </div> ' +
'                    <div id="action-button-container-right" class="w3-col s12 m6 l6"> ' +
'                        <div id="next-button-container" class="w3-col s12 m6 l6"> ' +
'                            <button id="next" class="action-buttons">Next</button> ' +
'                            <button id="submit" class="action-buttons">Submit</button> ' +
'                        </div> ' +
'                    </div> ' +
'                </div> ' +
'            </div> '

        var html = $.parseHTML(str)
        $(html).appendTo($this)

       Tempus.Forms.Controls.DSG.SRC.SRCWorkflowActionsButtons._attachEventHandler($this)
        //initialise the data array
        $this.data("options", {});

    });

});